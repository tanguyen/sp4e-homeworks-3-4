#include "fft.hh"
#include "my_types.hh"
#include <gtest/gtest.h>

/*****************************************************************/
TEST(FFT, transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    val = cos(k * i);
    //std::cout << val << " ";
  }

  Matrix<complex> res = FFT::transform(m);

  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);
    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else if (i == N - 1 && j == 0)
      ASSERT_NEAR(std::abs(val), N * N / 2, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }
}
/*****************************************************************/

TEST(FFT, inverse_transform) {
  UInt N = 512;
  Matrix<complex> m(N);

  //constant signal in Fourier domain
  Real k = 2 * M_PI / N;
  for (auto&& entry : index(m)) {
    auto& val = std::get<2>(entry);
    val = 1;
  }

  Matrix<complex> res = FFT::itransform(m);
  //check that we get a Dirac in the initial domain
  for (auto&& entry : index(res)) {
    int i = std::get<0>(entry);
    int j = std::get<1>(entry);
    auto& val = std::get<2>(entry);

    if (std::abs(val) > 1e-10)
      std::cout << i << "," << j << " = " << val << std::endl;

    if (i == 0 && j == 0)
      ASSERT_NEAR(std::abs(val), N*N, 1e-10);
    else
      ASSERT_NEAR(std::abs(val), 0, 1e-10);
  }

}
/*****************************************************************/

TEST(FFT, frequencies) {

  //no proper test (just printing)
  int N = 6;
  std::cout << "even case\n";
  Matrix<std::complex<int>> res = FFT::computeFrequencies(N);
  for (int i=0; i<N; i++){
    for (int j=0; j<N; j++){
      std::cout << i << "," << j << " = " << res(i,j) << std::endl;
    }
  }
  std::cout << std::endl;

  std::cout << "odd case\n";
  N = N+1;
  Matrix<std::complex<int>> res1 = FFT::computeFrequencies(N);
  for (int i=0; i<N; i++){
    for (int j=0; j<N; j++){
      std::cout << i << "," << j << " = " << res1(i,j) << std::endl;
    }
  }

}