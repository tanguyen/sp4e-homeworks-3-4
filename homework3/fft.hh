#ifndef FFT_HH
#define FFT_HH
/* ------------------------------------------------------ */
#include "matrix.hh"
#include "my_types.hh"
#include <fftw3.h>
/* ------------------------------------------------------ */

struct FFT {

  static Matrix<complex> transform(Matrix<complex>& m);
  static Matrix<complex> itransform(Matrix<complex>& m);

  static Matrix<std::complex<int>> computeFrequencies(int size);
};

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::transform(Matrix<complex>& m_in) {
  Matrix<complex> m_out(m_in.size());
  fftw_complex *in, *out;
  fftw_plan p;

  in = (fftw_complex*) m_in.data();
  out = (fftw_complex*) m_out.data();
  p = fftw_plan_dft_2d(m_in.size(),m_in.size(), in, out, FFTW_FORWARD, FFTW_ESTIMATE);

  fftw_execute(p);

  fftw_destroy_plan(p);

  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<complex> FFT::itransform(Matrix<complex>& m_in) {
  Matrix<complex> m_out(m_in.size());
  fftw_complex *in, *out;
  fftw_plan p;

  in = (fftw_complex*) m_in.data();
  out = (fftw_complex*) m_out.data();
  p = fftw_plan_dft_2d(m_in.size(),m_in.size(), in, out, FFTW_BACKWARD, FFTW_ESTIMATE);

  fftw_execute(p);

  fftw_destroy_plan(p);

  return m_out;
}

/* ------------------------------------------------------ */

inline Matrix<std::complex<int>> FFT::computeFrequencies(int size) {
  Matrix<std::complex<int>> res(size);
  int stop;

  std::vector<int> vec(size); //pattern that will be copied in the matrix
  if (size % 2 == 0) {  // even
    stop = size / 2 - 1;
  }
  else { // odd
    stop = (size - 1) / 2;
  }
  for (int i = 0; i <= stop; i++) { // fill the positive frequencies
    vec[i] = i;
  }
  int n = -1;
  for (int i = size-1; i > stop; i--) { // fill the negative frequencies
    vec[i] = n;
    n--;
  }
  //fill the matrix
  for (int i=0; i<size; i++){
    for (int j=0; j<size; j++){
      res(i,j).real(vec[i]);
      res(i,j).imag(vec[j]);
    }
  }

  return res;

}

#endif  // FFT_HH
