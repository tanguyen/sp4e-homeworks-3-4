#include "system.hh"
#include "compute_temperature.hh"
#include "material_points_factory.hh"
#include "fft.hh"
#include "my_types.hh"
#include <vector>
#include <gtest/gtest.h>
#include <math.h>

// Fixture class
class MaterialPoints : public ::testing::Test {

protected:
  void SetUp() override {
    size = 64;
    dt = 1.;
    for (UInt i=0; i<size*size; i++){
      //add particle
      auto p = MaterialPointsFactory::getInstance().createParticle();
      system.addParticle(std::move(p));
    }
    temperature = std::make_shared<ComputeTemperature>(dt);
  }

  System system;
  //UInt n_planets;
  UInt size;
  Real dt;

  std::shared_ptr<ComputeTemperature> temperature;
};

TEST_F(MaterialPoints, homogeneous) {
  //all initial temperatures are equal to zero and no heat rate by defaut
  Real hv = 0;
  Real temp = 25;
  //set temperatures and heat rates
  for (UInt i=0; i<size*size; i++){
    Particle& par = system.getParticle(i);
    auto& matpt = static_cast<MaterialPoint&>(par);
    matpt.getTemperature() = temp;
    matpt.getHeatRate() = hv;
  }
  //compute iteration
  temperature->compute(system);

  //verify temperature of all material points after 1 iteration
  for (UInt i=0; i<size*size; i++){
    Particle& par = system.getParticle(i);
    auto& matpt = static_cast<MaterialPoint&>(par);
    ASSERT_NEAR(matpt.getTemperature(), temp, 1e-15);
  }
}

TEST_F(MaterialPoints, sinusoidal) {
  //compute and assign hv and theta values for equally spaced x coordinates in [-1,1]
  Real L = 2.;
  Real alpha = pow(2.*M_PI/L,2.);
  Real delta = L/Real(size);
  Real hv;
  std::vector<Real> thetavec(size);
  Real x = -1.;
  UInt idx=0;
  for (UInt i=0; i<size; i++){
    thetavec[i] = sin(2.*M_PI*x/L);
    hv = alpha*thetavec[i];
    for (UInt j=0; j<size; j++){
      Particle& par = system.getParticle(idx);
      auto& matpt = static_cast<MaterialPoint&>(par);
      matpt.getTemperature() = thetavec[i];
      matpt.getHeatRate() = hv;
      idx++;
    }
    x += delta;
  }
  //compute iteration
  temperature->setDeltaX(delta);
  temperature->compute(system);

  //verify that the temperatures have not changed
  idx=0;
  for (UInt i=0; i<size; i++){
    for (UInt j=0; j<size; j++){
      Particle& par = system.getParticle(idx);
      auto& matpt = static_cast<MaterialPoint&>(par);
      ASSERT_NEAR(matpt.getTemperature(), thetavec[i], 1e-6);
      idx++;
    }
  }
}

TEST_F(MaterialPoints, lines) {
  //compute and assign hv and theta values for equally spaced x coordinates in [-1,1]
  Real L = 2.;
  Real delta = L/Real(size);
  std::vector<Real> thetavec(size);
  Real x = -1.;
  UInt idx=0;
  UInt div = size/4;
  std::cout << "div" << div << std::endl;
  //x<-1/2
  for (UInt i=0; i<div; i++){
    thetavec[i] = -x-1.;
    for (UInt j=0; j<size; j++){
      Particle& par = system.getParticle(idx);
      auto& matpt = static_cast<MaterialPoint&>(par);
      matpt.getTemperature() = thetavec[i];
      matpt.getHeatRate() = 0.;
      idx++;
    }
    x += delta;
  }
  //x = -1/2
  thetavec[div] = -x-1.;
  for (UInt j=0; j<size; j++) {
    Particle& par = system.getParticle(idx);
    auto& matpt = static_cast<MaterialPoint&>(par);
    matpt.getTemperature() = thetavec[div];
    matpt.getHeatRate() = -1.;
    idx++;
  }
  x += delta;

  //-1/2 < x < 1/2
  for (UInt i=div+1; i<3*div; i++){
    thetavec[i] = x;
    for (UInt j=0; j<size; j++){
      Particle& par = system.getParticle(idx);
      auto& matpt = static_cast<MaterialPoint&>(par);
      matpt.getTemperature() = thetavec[i];
      matpt.getHeatRate() = 0.;
      idx++;
    }
    x += delta;
  }

  //x = 1/2
  thetavec[3*div] = x;
  for (UInt j=0; j<size; j++) {
    Particle& par = system.getParticle(idx);
    auto& matpt = static_cast<MaterialPoint&>(par);
    matpt.getTemperature() = thetavec[3*div];
    matpt.getHeatRate() = 1.;
    idx++;
  }
  x += delta;


  // x > 1/2
  for (UInt i=3*div+1; i<size; i++){
    thetavec[i] = -x+1.;
    for (UInt j=0; j<size; j++){
      Particle& par = system.getParticle(idx);
      auto& matpt = static_cast<MaterialPoint&>(par);
      matpt.getTemperature() = thetavec[i];
      matpt.getHeatRate() = 0.;
      idx++;
    }
    x += delta;
  }

  //compute iteration
  temperature->setDeltaX(delta);
  temperature->setRho(1.);
  temperature->setC(1.);
  temperature->setKappa(1.);
  temperature->compute(system);

  //verify that the temperatures have not changed
  idx=0;
  for (UInt i=0; i<size; i++){
    for (UInt j=0; j<size; j++){
      Particle& par = system.getParticle(idx);
      auto& matpt = static_cast<MaterialPoint&>(par);
      ASSERT_NEAR(matpt.getTemperature(), thetavec[i], 1e-10);
      idx++;
    }

  }
}