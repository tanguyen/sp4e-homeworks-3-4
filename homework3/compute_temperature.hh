#ifndef __COMPUTE_TEMPERATURE__HH__
#define __COMPUTE_TEMPERATURE__HH__

/* -------------------------------------------------------------------------- */
#include "compute.hh"

//! Compute temperature (1 transient heat equation step)
class ComputeTemperature : public Compute {
// Constructors/Destructors
public:
  ComputeTemperature(Real timestep);
  // Methods
public:
  //! Set time step
  void setDeltaT(Real dt);
  //! Set grid spacing
  void setDeltaX(Real dx);
  //! set the heat capacity
  void setC(Real C);
  //! set the heat conductivity
  void setKappa(Real kappa);
  //! set the mass density
  void setRho(Real rho);
  //! Heat equation step implementation
  void compute(System& system) override;


private:
  Real dt=1.;
  Real dx=1.; //grid spacing (dx=dy)
  Real C=1.;
  Real kappa=1.;
  Real rho=1.;

};

/* -------------------------------------------------------------------------- */
#endif  //__COMPUTE_TEMPERATURE__HH__
