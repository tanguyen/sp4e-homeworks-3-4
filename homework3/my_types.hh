#ifndef __MY_TYPES_HH__
#define __MY_TYPES_HH__

/* -------------------------------------------------------------------------- */
#include <iostream>
#include <complex>

using UInt = unsigned int;
using Real = double;
using complex = std::complex<Real>;

template <typename D, typename S>
std::complex<D> cast(const std::complex<S> s)
{
  return std::complex<D>(s.real(), s.imag());
}

/* -------------------------------------------------------------------------- */
#define TO_IMPLEMENT                                                           \
  {                                                                            \
    std::cerr << std::endl                                                     \
              << std::endl                                                     \
              << "*********************************************************\n" \
              << "To be filled @ " << __FILE__ << ":" << __LINE__ << std::endl \
              << "*********************************************************\n" \
              << std::endl                                                     \
              << std::endl;                                                    \
    throw;                                                                     \
  }
/* -------------------------------------------------------------------------- */

#define EXERCISE_BEGIN_CORRECTION
#define EXERCISE_END_CORRECTION

#endif /* __MY_TYPES_HH__ */
