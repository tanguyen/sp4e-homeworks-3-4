#include "compute_temperature.hh"
#include "fft.hh"
#include "material_point.hh"
#include <cmath>
//#include "matrix.hh"

ComputeTemperature::ComputeTemperature(Real dt) : dt(dt) {}

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setDeltaT(Real dt) { this->dt = dt; }

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setDeltaX(Real dx) { this->dx = dx; }

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setC(Real C) { this->C = C; }

/* -------------------------------------------------------------------------- */
void ComputeTemperature::setKappa(Real kappa) { this->kappa = kappa; }

/* -------------------------------------------------------------------------- */

void ComputeTemperature::setRho(Real rho) { this->rho = rho; }

/* -------------------------------------------------------------------------- */
void ComputeTemperature::compute(System& system) {
  //fill the matrix of temperatures and heat source
  UInt size = std::sqrt(system.getNbParticles());
  Matrix<complex> theta(size);
  Matrix<complex> hv(size);
  UInt idx = 0;
  for (UInt i=0; i<size; i++) { //fill row by row
    for (UInt j=0; j<size; j++) {
      Particle& par = system.getParticle(idx);
      auto& matpt = static_cast<MaterialPoint&>(par);
      theta(i,j) = matpt.getTemperature();
      hv(i,j) = matpt.getHeatRate();
      idx++;
    }
  }

  //compute FFT
  Matrix<complex> thetaf = FFT::transform(theta);
  Matrix<complex> hvf = FFT::transform(hv);

  //compute update in frequency domain
  Matrix<std::complex<int>> q = FFT::computeFrequencies(size);
  Matrix<complex> qnorm = norm(q);
  qnorm *= pow(2.*M_PI/(Real(size)*dx),2.); //normalize frequencies
  thetaf *= kappa;
  Matrix<complex> dthetaf = hvf - thetaf*qnorm;
  dthetaf /= 1/(rho*C);

  //compute iFFT
  Matrix<complex> dtheta = FFT::itransform(dthetaf);

  //compute update
  dtheta/=Real(size); // normalize the dft
  dtheta *= dt;

  //store new temperature values in system
  std::cout << "dtheta" << std::endl;
  idx = 0;
  for (UInt i=0; i<size; i++) { //fill row by row
    for (UInt j=0; j<size; j++) {
      Particle& par = system.getParticle(idx);
      auto& matpt = static_cast<MaterialPoint&>(par);
      auto& temp = matpt.getTemperature();
      //apply update
      temp += dtheta(i,j).real();
      idx++;
    }
  }

}
/* -------------------------------------------------------------------------- */
