# MATH-611 Homework 3

Thiên-Anh Nguyen

## Overview

### Exercise 1: done

### Exercise 2: done

### Exercise 3: done

### Exercise 4:

1. done
2. done `TEST_F(MaterialPoints, homogeneous)`
3. done `TEST_F(MaterialPoints, sinusoidal)`
4. does not work `TEST_F(MaterialPoints, lines)`
5. not done
6. not done
	

## Exercise 1

Each MaterialPoint object corresponds to a single particle. The Matrix structure enables to store values (temperatures/heat rates) in a vector but using it as a square 2D matrix, i.e. the particles are distributed in a 2D grid. In the code the values are stored row by row (vector index = i*size + j) and the grid is square and regular (identical x-axis and y-axis spacing).

The wrapping FFT interface takes Matrix objects as arguments. 

In the MaterialPointsFactory the particles are stored as a ParticleList (a vector of shared pointers)

## Exercise 2

## Exercise 3

The tests are implemented in the **test_fft.cc** file.
To launch the tests, run the **test_fft** executable after compilation.

## Exercise 4

The tests are implemented in the **test_temperature.cc** file.
To launch the tests, run the **test_temperature** executable after compilation.

## Remarks

I forgot to use `MatrixIterator` to iterate through the matrices.

