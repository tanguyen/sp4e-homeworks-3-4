#include <string>
#include <pybind11/pybind11.h>

namespace py = pybind11;

#include "system_evolution.hh"
#include "compute.hh"
#include "compute_gravity.hh"
#include "compute_interaction.hh"
#include "compute_temperature.hh"
#include "compute_verlet_integration.hh"
#include "csv_writer.hh"
#include "particles_factory_interface.hh"
#include "material_points_factory.hh"
#include "ping_pong_balls_factory.hh"
#include "planets_factory.hh"

//trampoline
class PyParticlesFactoryInterface: public ParticlesFactoryInterface {
public:
  SystemEvolution& createSimulation(const std::string& fname, Real timestep) override {
    PYBIND11_OVERRIDE_PURE(
        SystemEvolution&, //return type
        ParticlesFactoryInterface, //parent class
        createSimulation, //name of function
        fname, timestep); //arguments
  }
};

class PyPlanetsFactory: public PlanetsFactory {
  SystemEvolution& createSimulation(const std::string& fname, Real timestep) override {
    PYBIND11_OVERRIDE(
        SystemEvolution&, //return type
        PlanetsFactory, //parent class
        createSimulation, //name of function
        fname, timestep); //arguments
  }
};

std::shared_ptr<ComputeGravity> get_shared_ptr() {
  return std::make_shared<ComputeGravity>();
}

PYBIND11_MODULE(pypart, m) {

  m.doc() = "pybind of the Particles project";

  // bind the routines here

  py::class_<System>(m, "System")
      .def(py::init<>());

  py::class_<SystemEvolution>(
      m, "SystemEvolution")
      .def_property("NSteps", nullptr, &SystemEvolution::setNSteps)
      .def_property("DumpFreq", nullptr, &SystemEvolution::setDumpFreq)
      .def_property("system", &SystemEvolution::getSystem, nullptr)
      .def("evolve", &SystemEvolution::evolve)
      .def("addCompute", &SystemEvolution::addCompute);

  py::class_<ParticlesFactoryInterface, PyParticlesFactoryInterface>(
      m, "ParticlesFactoryInterface")
      .def_property("system_evolution",&ParticlesFactoryInterface::getSystemEvolution, &ParticlesFactoryInterface::getSystemEvolution)
      .def("getInstance", &ParticlesFactoryInterface::getInstance,py::return_value_policy::reference)
      .def("createSimulation", py::overload_cast<const std::string &, Real, py::function>(
                                   &ParticlesFactoryInterface::createSimulation<py::function>),py::return_value_policy::reference);


  py::class_<PlanetsFactory,ParticlesFactoryInterface, PyPlanetsFactory>(
      m, "PlanetsFactory")
      .def("getInstance", &PlanetsFactory::getInstance,py::return_value_policy::reference);

  py::class_<PingPongBallsFactory,ParticlesFactoryInterface>(
      m, "PingPongBallsFactory")
      .def("getInstance", &PingPongBallsFactory::getInstance,py::return_value_policy::reference);

  py::class_<MaterialPointsFactory,ParticlesFactoryInterface>(
      m, "MaterialPointsFactory")
      .def("getInstance", &MaterialPointsFactory::getInstance,py::return_value_policy::reference);


  py::class_<Compute,std::shared_ptr<Compute>>(
      m, "Compute");
      //.def();

  py::class_<ComputeVerletIntegration,Compute,std::shared_ptr<ComputeVerletIntegration>>(
      m, "ComputeVerletIntegration")
      .def(py::init<Real>(),py::return_value_policy::reference) //constructor
      .def("addInteraction", &ComputeVerletIntegration::addInteraction);

  py::class_<ComputeTemperature,Compute,std::shared_ptr<ComputeTemperature>>(
      m, "ComputeTemperature")
      .def(py::init<>(), py::return_value_policy::reference) //constructor
      .def_property("conductivity", &ComputeTemperature::getConductivity, &ComputeTemperature::getConductivity) // give read write access (property) to member
      .def_property("L", &ComputeTemperature::getL, &ComputeTemperature::getL)
      .def_property("capacity", &ComputeTemperature::getCapacity, &ComputeTemperature::getCapacity)
      .def_property("density", &ComputeTemperature::getDensity, &ComputeTemperature::getDensity)
      .def_property("deltat", &ComputeTemperature::getDeltat, &ComputeTemperature::getDeltat);


  py::class_<ComputeInteraction,Compute,std::shared_ptr<ComputeInteraction>>(
        m, "ComputeInteraction");
        //.def();

  py::class_<ComputeGravity, ComputeInteraction, std::shared_ptr<ComputeGravity>>(
      m, "ComputeGravity")
      .def(py::init<>(),py::return_value_policy::reference) //constructor
      .def_property("G", &ComputeGravity::getG, &ComputeGravity::setG);
      //.def("setG", &ComputeGravity::setG);
  m.def("get_shared_ptr", &get_shared_ptr,"get shared pointer for ComputeGravity");

  py::class_<CsvWriter, Compute, std::shared_ptr<CsvWriter>>(
      m, "CsvWriter")
      .def(py::init<const std::string &>(),py::return_value_policy::reference) //constructor
      .def("write", &CsvWriter::write);


}
