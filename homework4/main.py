#!/bin/env python3

import os, os.path
import sys
import shutil
import numpy as np
import argparse
import pypart
from scipy.optimize import fmin
from pypart import MaterialPointsFactory, ParticlesFactoryInterface
from pypart import PingPongBallsFactory
from pypart import PlanetsFactory
from pypart import CsvWriter
from pypart import ComputeTemperature
from pypart import ComputeGravity
from pypart import ComputeVerletIntegration

# help(pypart.SystemEvolution)

def cpp_interface(nsteps,freq,filename,particle_type,timestep):

    #print(pypart.__file__)

    if particle_type == "planet":
        PlanetsFactory.getInstance()
    elif particle_type == "ping_pong":
        PingPongBallsFactory.getInstance()
    elif particle_type == "material_point":
        MaterialPointsFactory.getInstance()
    else:
        print("Unknown particle type: ", particle_type)
        sys.exit(-1)

    factory = ParticlesFactoryInterface.getInstance()

    def createComputes(self, timestep):

        if particle_type == "planet":
    
            try:
                compute_grav = ComputeGravity()
                #compute_grav = pypart.get_shared_ptr()
                compute_verlet = ComputeVerletIntegration(timestep)
        
                G = 6.67384e-11 # m^3 * kg^-1 * s^-2
                UA = 149597870.700 # km
                earth_mass = 5.97219e24 # kg
                G /= (UA * 1e3)**3 # UA^3 * kg^-1 * s^-2
                G *= earth_mass    # UA^3 * earth_mass^-1 * s^-2
                G *= (60*60*24)**2 # UA^3 * earth_mass^-1 * day^-2
        
                #compute_grav.setG(G)
                compute_grav.G = G;
                compute_verlet.addInteraction(compute_grav)
                self.system_evolution.addCompute(compute_verlet)
            
            except Exception as e:
                help(pypart.ComputeGravity)
                raise e
        
        elif particle_type == 'material_point':
        
            try:
                compute_temp = ComputeTemperature()
                compute_temp.conductivity = 1
                compute_temp.L = 2
                compute_temp.capacity = 1
                compute_temp.density = 1
                compute_temp.deltat = 1
                self.system_evolution.addCompute(compute_temp)
            except Exception as e:
                help(compute_temp)
                raise e

    evol = factory.createSimulation(filename, timestep, createComputes)

    dumper = CsvWriter("out.csv")
    dumper.write(evol.system)
    
    #evol.setNSteps(nsteps)
    #evol.setDumpFreq(freq)
    evol.NSteps = nsteps
    evol.DumpFreq  = freq
    evol.evolve()
    
def readPositions(planet_name,directory):
    positions = []
    files = os.listdir(directory)
    files.sort()
    for name in files:
        file = os.path.join(directory,name)
        if os.path.isfile(file):
            f = open(file, 'r')
            for line in f:
                if line.split()[-1]==planet_name:
                    coord = [float(x) for x in line.split()[:3]] 
                    positions.append(coord)
    return np.array(positions)

def computeError(positions, positions_ref):
    return np.linalg.norm(positions-positions_ref)

def generateInput(scale,planet_name,input_filename,output_filename):
    fin = open(input_filename, 'r')
    fout = open(output_filename, 'w')
    for line in fin:
        if line.split()[-1]!=planet_name:
            fout.write(line)
        else:
            l = line.split()
            velocity = [float(v)*scale for v in l[3:6]]
            l[3:6] = [str(v) for v in velocity]
            fout.write(" ".join(l) + "\n")
    fout.close()
            
def launchParticles(input,nb_step,freq):
    cpp_interface(nb_step,freq,input,"planet",1) 
    
def emptyFolder(folder):
    for filename in os.listdir(folder):
        file_path = os.path.join(folder, filename)
        try:
            if os.path.isfile(file_path) or os.path.islink(file_path):
                os.unlink(file_path)
            elif os.path.isdir(file_path):
                shutil.rmtree(file_path)
        except Exception as e:
            print('Failed to delete %s. Reason: %s' % (file_path, e))
            
def rename_file(filename):
    base = os.path.basename(filename)
    folder = os.path.dirname(filename)
    new_name = os.path.join(folder,os.path.splitext(base)[0] + '_copy' + os.path.splitext(base)[-1])
    os.rename(filename,new_name)
    return new_name

def runAndComputeError(scale,planet_name,input,nb_steps,freq):
    folder = os.path.dirname(os.path.abspath(__file__))
    try:
        scale = scale[0]
    except: 
        pass
    print("scale "+str(scale))
    # generate scaled input
    scaled_input = os.path.join(os.path.dirname(input),"init_scaled.csv")
    # input = rename_file(input)
    generateInput(scale,planet_name,input,scaled_input)
    # check if dumps directory exists and is empty
    if os.path.isdir(os.path.join(folder,'dumps')):
        emptyFolder(os.path.join(folder,'dumps'))
    else:
        os.mkdir(os.path.join(folder,'dumps'))
    # launch the particles code
    launchParticles(scaled_input,nb_steps,freq)
    # read the generated and reference positions
    positions = readPositions(planet_name,'./dumps')
    positions_ref_path = os.path.join(os.path.dirname(folder),'trajectories')
    positions_ref = readPositions(planet_name, positions_ref_path)
    # rcompute and return error
    error = computeError(positions, positions_ref)
    print("error " + str(error))
    return error

def main(nsteps, freq, filename, scale_init, planet_name, maxit):
    # generate first "init_scaled.csv" file
    # output = os.path.join(os.path.dirname(filename),"init_scaled.csv")
    # generateInput(1,planet_name,filename,output)
    
    #launch optimization
    input_file = "init.csv"
    args=(planet_name,input_file,nsteps,freq)
    scale_opt = fmin(runAndComputeError,scale_init,args,maxiter=maxit)
    print("Optimal scale : " + str(scale_opt))
     

# if __name__ == "__main__":

#     parser = argparse.ArgumentParser(description='Particles code')
#     parser.add_argument('nsteps', type=int,
#                         help='specify the number of steps to perform')
#     parser.add_argument('freq', type=int,
#                         help='specify the frequency for dumps')
#     parser.add_argument('filename', type=str,
#                         help='start/input filename')
#     parser.add_argument('particle_type', type=str,
#                         help='particle type')
#     parser.add_argument('timestep', type=float,
#                         help='timestep')
    
#     args = parser.parse_args()
#     nsteps = args.nsteps
#     freq = args.freq
#     filename = args.filename
#     particle_type = args.particle_type
#     timestep = args.timestep

#     main(nsteps,freq,filename,particle_type,timestep)

if __name__ == "__main__":
    parser = argparse.ArgumentParser(description='Scale optimization')
    parser.add_argument('nsteps', type=int,
                        help='specify the number of steps to perform')
    parser.add_argument('freq', type=int,
                        help='specify the frequency for dumps')
    parser.add_argument('filename', type=str,
                        help='start/input filename')
    parser.add_argument('scale_init', type=str,
                        help='initial scale value')
    parser.add_argument('planet_name', type=str,
                        help='planet name')
    parser.add_argument('maxiter', type=float,
                        help='maximum number of iteration')
    
    args = parser.parse_args()
    nsteps = args.nsteps
    freq = args.freq
    filename = args.filename
    scale_init = args.scale_init
    planet_name = args.planet_name
    maxit = args.maxiter
    print(args.nsteps)
    
    main(nsteps, freq, filename, scale_init, planet_name, maxit)
    

# parameters
# nsteps = 365
# freq = 1
# input_file  = "init.csv"
# particle_type = "planet"
# timestep = 1
# planet_name="mercury"

