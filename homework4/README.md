Nguyen Thien-Anh
# SP4E homework 4

## Part 1: Pybind

The `CMakeLists.txt` file has been modified to work on my personal machine.

### Exercise 1
Answer to question 2:
The role of the overloading createSimulation method is to set the createComputes attribute (with the functor argument) before calling the original createSimulation method. The createComputes attribute is used by the daughter classes to associate desired forces to the simulation (i.e. create the right Compute_x objects). If no functor is given to createSimulation, createDefaultComputes will be called to associate the default forces according to the particle type (for example, gravity for planets and contact for ping-pong balls).


### Exercise 2

### Exercise 3

## Part 2: Particle trajectory optimization

### Exercise 4

### Exercise 5

`init.csv` should be copied into `cmake-build-debug` folder, and `main.py` should be launched from this folder. The reference trajectories should be in a folder called `trajectories` in the parent folder.

To launch the optimization with the homeworks parameters use command `python3 main.py 365 1 init.csv 1 'mercury' <maxiter>` with `<maxiter>` an integer corresponding to the maximum number of iterations in the optimization process.

usage: main.py [-h] nsteps freq filename scale_init planet_name maxiter

positional arguments:

-   nsteps,       specify the number of steps to perform
-   freq,         specify the frequency for dumps
-   filename,     start/input filename
-   scale_init,   initial scale value
-   planet_name,  planet name
-   maxiter,      maximum number of iteration

To print this help use command `python3 main.py -h`.



